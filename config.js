module.exports = {
  brew: [
    'mas',
    'wget --enable-iri',
    'yarn',
    'autojump'
  ],
  cask: [
    'google-chrome',
    'iterm2',
    'docker',
    'slack',
    'sublime-text',
    'stack',
    'mamp',
    'transmit',
    'spotify',
    'tower'
  ],
  gem: [],
  npm: [
    'pm2',
  ],
  mas: [
    //497799835 //x-code
    926036361,
    407963104,
    918858936,
    803453959,
    747648890
  ]
};


